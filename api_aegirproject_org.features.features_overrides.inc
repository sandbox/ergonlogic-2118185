<?php
/**
 * @file
 * api_aegirproject_org.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function api_aegirproject_org_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable
  $overrides["variable.api_default_project.value"] = 'aegir';
  $overrides["variable.site_frontpage.value"] = 'api/aegir';
  $overrides["variable.site_name.value"] = 'api10.api.local';

 return $overrides;
}
