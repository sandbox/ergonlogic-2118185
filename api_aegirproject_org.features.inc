<?php
/**
 * @file
 * api_aegirproject_org.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function api_aegirproject_org_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function api_aegirproject_org_strongarm_alter(&$data) {
  if (isset($data['api_default_project'])) {
    $data['api_default_project']->value = 'aegir'; /* WAS: 'drupal' */
  }
  if (isset($data['site_frontpage'])) {
    $data['site_frontpage']->value = 'api/aegir'; /* WAS: 'api/drupal/7' */
  }
  if (isset($data['site_name'])) {
    $data['site_name']->value = 'api10.api.local'; /* WAS: 'DrupalAPI' */
  }
}
