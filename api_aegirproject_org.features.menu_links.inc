<?php
/**
 * @file
 * api_aegirproject_org.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function api_aegirproject_org_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_aegir:api/aegir
  $menu_links['main-menu_aegir:api/aegir'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'api/aegir',
    'router_path' => 'api/aegir',
    'link_title' => 'Aegir',
    'options' => array(
      'attributes' => array(
        'title' => 'Aegir API documentation',
      ),
      'identifier' => 'main-menu_aegir:api/aegir',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_drush:api/drush
  $menu_links['main-menu_drush:api/drush'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'api/drush',
    'router_path' => 'api/drush',
    'link_title' => 'Drush',
    'options' => array(
      'attributes' => array(
        'title' => 'Drush API documentation',
      ),
      'identifier' => 'main-menu_drush:api/drush',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Aegir');
  t('Drush');


  return $menu_links;
}
