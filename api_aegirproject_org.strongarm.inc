<?php
/**
 * @file
 * api_aegirproject_org.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function api_aegirproject_org_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'api_default_core_compatibility';
  $strongarm->value = '6.x';
  $export['api_default_core_compatibility'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_default';
  $strongarm->value = 'eldir';
  $export['theme_default'] = $strongarm;

  return $export;
}
