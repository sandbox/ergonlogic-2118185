core = 7.x
api = 2



;;;;;;;;;;;;;;;;;;;;;;;
;;   Aegir 6.x-1.x   ;;
;;;;;;;;;;;;;;;;;;;;;;;

libraries[hostmaster][download][type] = git
libraries[hostmaster][download][url] = http://git.drupal.org/project/hostmaster.git
libraries[hostmaster][download][branch] = 6.x-1.x
libraries[hostmaster][destination] = .sources
;; Drupal API data
libraries[hostmaster][api_project_name] = aegir

libraries[provision][download][type] = git
libraries[provision][download][url] = http://git.drupal.org/project/provision.git
libraries[provision][download][branch] = 6.x-1.x
libraries[provision][destination] = .sources
;; Drupal API data
libraries[provision][api_project_name] = aegir



;;;;;;;;;;;;;;;;;;;;;;;
;;   Aegir 6.x-2.x   ;;
;;;;;;;;;;;;;;;;;;;;;;;

libraries[hostmaster2][download][type] = git
libraries[hostmaster2][download][url] = http://git.drupal.org/project/hostmaster.git
libraries[hostmaster2][download][branch] = 6.x-2.x
libraries[hostmaster2][destination] = .sources
;; Drupal API data
libraries[hostmaster2][api_project_name] = aegir
libraries[hostmaster2][api_project_title] = Aegir
libraries[hostmaster2][api_project_type] = module
libraries[hostmaster2][api_preferred] = true

libraries[eldir2][download][type] = git
libraries[eldir2][download][url] = http://git.drupal.org/project/eldir.git
libraries[eldir2][download][branch] = 6.x-2.x
libraries[eldir2][destination] = .sources
;; Drupal API data
libraries[eldir2][api_project_name] = aegir

libraries[hosting2][download][type] = git
libraries[hosting2][download][url] = http://git.drupal.org/project/hosting.git
libraries[hosting2][download][branch] = 6.x-2.x
libraries[hosting2][destination] = .sources
;; Drupal API data
libraries[hosting2][api_project_name] = aegir

libraries[provision2][download][type] = git
libraries[provision2][download][url] = http://git.drupal.org/project/provision.git
libraries[provision2][download][branch] = 6.x-2.x
libraries[provision2][destination] = .sources
;; Drupal API data
libraries[provision2][api_project_name] = aegir



;;;;;;;;;;;;;;;;;;;;;;;
;;   Aegir 7.x-3.x   ;;
;;;;;;;;;;;;;;;;;;;;;;;

libraries[hostmaster3][download][type] = git
libraries[hostmaster3][download][url] = http://git.drupal.org/project/hostmaster.git
libraries[hostmaster3][download][branch] = 7.x-3.x
libraries[hostmaster3][destination] = .sources
;; Drupal API data
libraries[hostmaster3][api_project_name] = aegir

libraries[eldir3][download][type] = git
libraries[eldir3][download][url] = http://git.drupal.org/project/eldir.git
libraries[eldir3][download][branch] = 7.x-3.x
libraries[eldir3][destination] = .sources
;; Drupal API data
libraries[eldir3][api_project_name] = aegir

libraries[hosting3][download][type] = git
libraries[hosting3][download][url] = http://git.drupal.org/project/hosting.git
libraries[hosting3][download][branch] = 7.x-3.x
libraries[hosting3][destination] = .sources
;; Drupal API data
libraries[hosting3][api_project_name] = aegir

libraries[provision3][download][type] = git
libraries[provision3][download][url] = http://git.drupal.org/project/provision.git
libraries[provision3][download][branch] = 7.x-3.x
libraries[provision3][destination] = .sources
;; Drupal API data
libraries[provision3][api_project_name] = aegir

