core = 7.x
api = 2



;;;;;;;;;;;;;;;;;;;
;;   Drush 5.x   ;;
;;;;;;;;;;;;;;;;;;;

libraries[drush5][download][type] = git
libraries[drush5][download][url] = https://github.com/drush-ops/drush.git
libraries[drush5][download][branch] = 5.x
libraries[drush5][destination] = .sources
;; Drupal API data
libraries[drush5][api_project_name] = drush



;;;;;;;;;;;;;;;;;;;
;;   Drush 6.x   ;;
;;;;;;;;;;;;;;;;;;;

libraries[drush6][download][type] = git
libraries[drush6][download][url] = https://github.com/drush-ops/drush.git
libraries[drush6][download][branch] = 6.x
libraries[drush6][destination] = .sources
;; Drupal API data
libraries[drush6][api_project_name] = drush
libraries[drush6][api_project_title] = Drush
libraries[drush6][api_project_type] = module
libraries[drush6][api_preferred] = true



;;;;;;;;;;;;;;;;;;;
;;   Drush 7.x   ;;
;;;;;;;;;;;;;;;;;;;

libraries[drush7][download][type] = git
libraries[drush7][download][url] = https://github.com/drush-ops/drush.git
libraries[drush7][download][branch] = master
libraries[drush7][destination] = .sources
;; Drupal API data
libraries[drush7][api_project_name] = drush

