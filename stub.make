api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7.25

projects[drupalapi][download][type] = git
projects[drupalapi][download][url] = http://git.drupal.org/project/drupalapi.git
projects[drupalapi][download][branch] = dev/2129501
projects[drupalapi][type] = profile
projects[drupalapi][patch][] = http://drupal.org/files/issues/add_sources.patch
projects[drupalapi][patch][] = http://drupal.org/files/issues/add_feature.patch

projects[api_aegirproject_org][subdir] = custom
projects[api_aegirproject_org][type] = module
projects[api_aegirproject_org][download][type] = git
projects[api_aegirproject_org][download][url] = http://git.drupal.org/sandbox/ergonlogic/2118185.git
projects[api_aegirproject_org][download][branch] = master

projects[] = eldir

